FROM python:3.6-slim

ADD . /app
WORKDIR /app

ENV BOT_API_TOKEN token

RUN pip install -r requirements.txt &&\
    adduser --disabled-password --gecos '' myuser

USER myuser

CMD python main.py