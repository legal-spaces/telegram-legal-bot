import os

from legal_spaces_bot import start_bot

PROXY_URL = os.getenv("PROXY_URL")
PROXY_USER = os.getenv("PROXY_USER")
PROXY_PASS = os.getenv("PROXY_PASS")


if __name__ == "__main__":
    start_bot(proxy_url=PROXY_URL, proxy_user=PROXY_USER, proxy_pass=PROXY_PASS)
