import os
import sqlite3

import dropbox
import gensim
import pymystem3
from many_stop_words import get_stop_words

from legal_spaces_bot import log

DBX_TOKEN = os.getenv("DBX_TOKEN")
MODEL = os.getenv("MODEL")
RES_FOLDER = "resources"


def download_res():
    if not os.path.exists(RES_FOLDER):
        os.mkdir(RES_FOLDER)

    dbx = dropbox.Dropbox(DBX_TOKEN)

    for entry in dbx.files_list_folder("/{}".format(MODEL)).entries:
        with open(os.path.join(RES_FOLDER, entry.name), "wb") as f:
            _, res = dbx.files_download("/{}/{}".format(MODEL, entry.name))
            data = res.content
            f.write(data)


class TextCleaner:
    def __init__(self, min_len):
        log.debug("Initializing TextCleaner")
        self.stem = pymystem3.Mystem(entire_input=False)
        self.stopwords = get_stop_words("ru")
        self.min_len = min_len

    # TODO: add phraser
    def _lemm_and_filter(self, text, min_len=3):
        word_filter = (
            lambda word: word not in self.stopwords and len(word) > self.min_len
        )
        return " ".join(list(filter(word_filter, self.stem.lemmatize(text))))

    def clean(self, text):
        log.debug("Filtering text")
        return self._lemm_and_filter(text)


class LSEmbeddings:
    def __init__(self, min_text_len=30, min_token_len=3):
        download_res()
        self.doc2vec_path = os.path.join(RES_FOLDER, "doc2vec.model")
        self.db_path = os.path.join(RES_FOLDER, "data.db")
        assert os.path.exists(self.db_path) and os.path.exists(self.doc2vec_path)
        self.model = gensim.models.doc2vec.Doc2Vec.load(self.doc2vec_path)
        self.cleaner = TextCleaner(min_token_len)
        self.min_text_len = min_text_len

    def _get_code_info(self, sims, n=5):
        connection = sqlite3.connect(self.db_path)
        query = f"SELECT number, name FROM data WHERE `index` in ({','.join([str(x[0]) for x in sims])})"
        cursor = connection.execute(query)
        data = cursor.fetchmany(n)
        cursor.close()
        return data

    def _get_embedding(self, text):
        return self.model.infer_vector(text)

    def _get_most_simular(self, text):
        log.debug("Calculating text embedding")
        emb = self._get_embedding(text)
        log.debug("Finding most simular docs in model")
        sims = self.model.docvecs.most_similar([emb], topn=100)
        log.debug("Selecting simular articles from db")
        return self._get_code_info(sims)

    def calculate(self, text):
        log.debug("New calculation started")
        result = None
        if text:
            text = self.cleaner.clean(text)
            if len(text) > self.min_text_len or len(set(text)) > 10:
                result = self._get_most_simular(text)
        return result
