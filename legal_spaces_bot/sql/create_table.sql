CREATE TABLE IF NOT EXISTS feedback
(
  id                          SERIAL PRIMARY KEY,
  ts                          VARCHAR,
  nickname                    VARCHAR,
  text                        VARCHAR,
  answer                      VARCHAR,
  feedback                    VARCHAR
);