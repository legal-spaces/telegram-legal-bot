import os

from telegram import InlineKeyboardButton, InlineKeyboardMarkup, ParseMode
from telegram.ext import (
    CallbackQueryHandler,
    Dispatcher,
    Filters,
    MessageHandler,
    Updater,
)

import legal_spaces_bot.messages as messages
from legal_spaces_bot.feedback import FeedbackDBWrapper
from legal_spaces_bot.processor.text import LSEmbeddings

lse = LSEmbeddings()
feedback_db = FeedbackDBWrapper(os.getenv("DATABASE_URL"))


def format_reply_msg(data):
    msg = [messages.MSG_INTRO]
    for number, name in data:
        msg.append("*Ст. {}* {}".format(number, name))
    msg.append("\n")
    msg.append(messages.FEEDBACK_REQUEST)
    return "\n".join(msg)


def format_reply_keybord(data):
    keyboard, row = [], []
    for number, _ in data:
        row.append(
            InlineKeyboardButton("Ст. {}".format(number), callback_data=str(number))
        )
    keyboard.append(row)
    keyboard.append(
        [InlineKeyboardButton(messages.ALL_WRONG, callback_data=messages.ALL_WRONG)]
    )
    return InlineKeyboardMarkup(keyboard)


def calc(bot, update):
    data = lse.calculate(update.message.text)
    keyboard = None
    msg = messages.NOT_ENOUGH_WORDS

    if data:
        msg = messages.NOTHING_FOUND

        if len(data) > 0:
            msg = format_reply_msg(data)
            keyboard = format_reply_keybord(data)

    update.message.reply_text(
        text=msg,
        # chat_id=update.message.chat_id,
        parse_mode=ParseMode.MARKDOWN,
        reply_markup=keyboard,
    )


def button(bot, update):
    query = update.callback_query
    new_text = query.message.text.replace(messages.FEEDBACK_REQUEST, "")

    bot.edit_message_text(
        text=new_text,
        chat_id=query.message.chat_id,
        message_id=query.message.message_id,
        parse_mode=ParseMode.MARKDOWN,
    )

    bot.send_message(
        text=messages.THANKS,
        chat_id=query.message.chat_id,
        parse_mode=ParseMode.MARKDOWN,
    )


def start_bot(**kwargs):
    REQUEST_KWARGS = None
    if kwargs.get("proxy_url"):
        REQUEST_KWARGS = {"proxy_url": "socks5://{}".format(kwargs.get("proxy_url"))}
        if kwargs.get("proxy_user"):
            REQUEST_KWARGS["urllib3_proxy_kwargs"] = {
                "username": kwargs.get("proxy_user"),
                "password": kwargs.get("proxy_pass"),
            }

    updater = Updater(
        token=os.environ.get("BOT_API_TOKEN"), request_kwargs=REQUEST_KWARGS
    )

    updater.dispatcher.add_handler(MessageHandler(Filters.text, calc))
    updater.dispatcher.add_handler(CallbackQueryHandler(button))

    updater.start_polling()
    updater.idle()
