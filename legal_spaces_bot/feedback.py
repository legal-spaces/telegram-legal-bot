from legal_spaces_bot import log

import pkg_resources
import psycopg2
import psycopg2.extensions


class LoggingCursorFactory(psycopg2.extensions.cursor):
    def execute(self, sql, args=None):
        log.debug(self.mogrify(sql, args).decode("utf-8"))
        try:
            psycopg2.extensions.cursor.execute(self, sql, args)
        except Exception as e:
            log.error("{}: {}".format(e.__class__.__name__, e))
            raise


class CursorManager:
    def __init__(self, url):
        self.url = url

    def __enter__(self):
        log.info("Connecting to database")
        self.connection = psycopg2.connect(
            self.url, cursor_factory=LoggingCursorFactory
        )
        log.info("Connection to database established")
        self.cursor = self.connection.cursor()
        return self.cursor

    def __exit__(self, exc_type, exc_val, exc_tb):
        self.connection.close()
        log.info("Connection to database closed")


class DBWrapper:
    def __init__(self, url):
        log.info("Connecting to database")
        self.connection = psycopg2.connect(url, cursor_factory=LoggingCursorFactory)
        self.connection.autocommit = True
        log.info("Connection to database established")
        self._templates = self.load_templates()

    def load_templates(self):
        templates = {}
        for file_name in pkg_resources.resource_listdir(__name__, "sql"):
            log.debug("Loading sql template: {}".format(file_name))
            resource_path = "/".join(("sql", file_name))
            template = pkg_resources.resource_string(__name__, resource_path)
            templates[file_name.replace(".sql", "")] = template.decode("utf-8")
        return templates

    def map2col(self, cursor, data):
        """Maps all rows to column names. Returns list[dict]"""
        return [{k[0]: v for k, v in zip(cursor.description, d)} for d in data]


class FeedbackDBWrapper(DBWrapper):
    def __init__(self, url):
        super(FeedbackDBWrapper, self).__init__(url)
        self._create_table()

    def _create_table(self):
        with self.connection.cursor() as cursor:
            cursor.execute(self._templates.get("create_table"))
    
    def insert_new_data(self, data:list):
        

        with self.connection.cursor() as cursor:
            insert_str = "(DEFAULT, %s, %s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s,%s)"
            args_str = ",".join(cursor.mogrify(insert_str, x.tup()).decode("utf-8") for x in data)
            cursor.execute(self._templates.get("insert_values") + args_str) 
