import logging
import os
import sys

log_level = os.getenv("LOG_LEVEL", "DEBUG")

log = logging.getLogger()
log.setLevel(log_level)

handler = logging.StreamHandler(sys.stdout)
handler.setLevel(log_level)
fmt_str = "[{levelname} {process:d}:{thread:d} {asctime}] {module}.{funcName}:{lineno} {message}"
formatter = logging.Formatter(fmt_str, style="{")
handler.setFormatter(formatter)
log.addHandler(handler)

from legal_spaces_bot.bot import start_bot
__all__ = ["start_bot"]
